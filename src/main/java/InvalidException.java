/**
 * Exceção de quando o numero de valores é diferente do numero de pesos.
 */
public class InvalidException extends Exception {

    /**
     * Costructor da Exceção
     */
    public InvalidException(){
        System.out.println("Valores de intruduzidos incorretos (valores e pesos)!");
    }
}
