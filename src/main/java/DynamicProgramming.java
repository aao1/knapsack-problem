import java.util.Stack;

/**
 * Class que reprensenta a metodologia de Dynamic Programing
 */
public class DynamicProgramming {
    /**
     * Capacidade máxima da mochila
     */
    private int maxWeight;

    /**
     * Array de pesos dos objetos
     */
    private int weights[];

    /**
     * Array de valores dos objetos
     */
    private int values[];

    /**
     * Número de objetos
     */
    private int n;

    /**
     * Construtor da class {@link DynamicProgramming}
     *
     * @param maxWeight capacidade máxima da mochila
     * @param weights   array de pesos dos objetos
     * @param values    array de valores dos objetos
     * @param n         número de objetos
     * @throws InvalidException {@inheritDoc}
     */
    public DynamicProgramming(int maxWeight, int[] weights, int[] values, int n) throws InvalidException {
        if (weights.length != values.length) {
            throw new InvalidException();
        }
        this.maxWeight = maxWeight;
        this.weights = weights;
        this.values = values;
        this.n = n;
    }

    /**
     * Algoritmo da metodologia de Dynamic Programming
     *
     * @return Stack com os objetos que são adicionados na mochila com o maior valor possivel
     */
    public Stack<Integer> knapsackProblem() {
        int line, col; //Linha e coluna
        int matrix[][] = new int[this.n + 1][this.maxWeight + 1]; //Matriz

        //Stack que guarda os elementos escolhidos
        Stack<Integer> stackResults = new Stack<>();

        //Preenchimento da matriz
        for (line = 0; line <= this.n; line++) {
            for (col = 0; col <= this.maxWeight; col++) {
                if (line == 0 || col == 0) {
                    matrix[line][col] = 0; //Colocar a primeira linha e coluna a 0
                } else if (this.weights[line - 1] <= col) {
                    matrix[line][col] = Math.max(this.values[line - 1] + matrix[line - 1][col - this.weights[line - 1]], matrix[line - 1][col]); //Calcular o valor máximo
                } else {
                    matrix[line][col] = matrix[line - 1][col];
                }
            }
        }

        //Colocar linha e coluna de forma a conseguir a ultima célula da matriz
        line = this.n;
        col = this.maxWeight;

        //Adicionar os indices dos elementos escolhidos à stack
        while (line != 0 || col != 0) {
            if (matrix[line][col] != matrix[--line][col]) {
                stackResults.add(line+1);
                col -= this.weights[line];
            }
        }

        return stackResults;
    }

    /**
     * Main class
     *
     * @param args Argumentos que são passados
     * @throws InvalidException {@inheritDoc}
     */
    public static void main(String[] args) throws InvalidException {
        //Lista de Valores
        int val[] = new int[]{100, 60, 120};

        //Lista de Pesos
        int wt[] = new int[]{20, 10, 30};

        //Peso limite da mochila
        int W = 50;

        //Numero de elementos
        int n = val.length;

        //Execução do algoritmo
        Stack results=(new DynamicProgramming(W,wt,val,n)).knapsackProblem();

        //Imprimir os id dos objetos
        while (!results.isEmpty()){
            System.out.println(results.pop());
        }
    }
}
